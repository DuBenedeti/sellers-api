<?php

namespace App\Console;

use App\Models\Sale;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function (){
            // Buscar soma das vendas do dia atual
            $sale = Sale::selectRaw('SUM(sale_value) as sale_value')
                ->where('created_at', '>', date('Y-m-d 00:00:00'))
                ->where('created_at', '<', date('Y-m-d 23:59:59'))
                ->first();

            //código para enviar email
            Mail::send(
                'email_template.sale-template',
                [
                    'sale' => $sale
                ],
                function ($m) use ($sale) {
                    $m->from('teste@teste.com.br', $sale);
                    $m->to('teste@teste.com.br')->subject('Relatório de vendas - ' . date('d-m-Y'));
                }
            );
        })->dailyAt('21:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
