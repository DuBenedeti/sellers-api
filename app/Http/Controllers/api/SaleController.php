<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Sale;
use App\Models\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SaleController extends Controller
{
    private const COMMISSION_PERCENT = 8.5 / 100; 

    public function index(Request $request){
                
        $sale =  Sale::where('sellers_id', $request->sellers_id)
        ->leftJoin('sellers', 'sellers.id', 'sale.sellers_id')
        ->get();

        return json_encode($sale);

    }

    public function store(Request $request){

        $rules = [
            'sellers_id' => ['required', 'integer'],
            'sale_value' => ['required', 'numeric'],
        ];
        $messages = [
            'sellers_id.required' => 'O nome é obrigatório',
            'sale_value.required' => 'O valor da venda é obrigatório',
            'sellers_id.integer' => 'O id do vendedor deve ser numérico e não decimal',
            'sale_value.numeric' => 'O valor da venda deve ser numérico',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if(!empty($validator->messages()->messages())){
            return response(json_encode($validator->messages()->messages()), 400);
        }

        $sale = $request->all();
        $sale['sale_value'] = (double) round(str_replace(',', '.', $sale['sale_value']), 2);
        $commission = ($sale['sale_value'] * self::COMMISSION_PERCENT);
        $sale['commission'] = round($commission, 2);

        try {
            
            $sale = Sale::create($sale);
            $sale['sellers_id'] = $sale->seller->id;
            
            return json_encode(response($sale, 201));

        } catch (\Throwable $th) {
            return response('Erro para cadastrar venda', 400);
        }

    }
}
