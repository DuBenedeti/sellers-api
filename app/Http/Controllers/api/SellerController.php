<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SellerController extends Controller
{
    public function index(){
        try {
            $sellers = Seller::leftJoin('sale', 'sellers.id', 'sale.sellers_id')
            ->select(DB::raw('sellers.id, sellers.name, sellers.email, SUM(sale.commission) as commission'))
            ->groupBy('sellers.id')->get();
            return response()->json($sellers);
            
        } catch (\Throwable $th) {
            return json_encode(response('Erro para listar vendedores', 400));
        }
    }

    public function store(Request $request){

        $rules = [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'max:255', 'email'],
        ];
        $messages = [
            'name.required' => 'O nome é obrigatório',
            'name.max' => 'Tamanho de caracteres excedido',
            'email.required' => 'O e-mail é obrigatório',
            'email.email' => 'Você não digitou um e-mail válido',
            'email.max' => 'Tamanho de caracteres excedido',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if(!empty($validator->messages()->messages())){
            return json_encode(response($validator->messages()->messages(), 400));
        }

        try {
            $seller = Seller::create($request->all());
            return json_encode(response($seller, 201));
        } catch (\Throwable $th) {
            return json_encode(response('Erro para cadastrar vendedor', 400));
        }

    }
}
