API Desafio Técnico Tray

Requisitos
- PHP 8.1
- Docker 

Configurar .env
- copiar .env.example para um novo arquivo .env
- php artisan key:generate para gerar a APP_KEY
- alterar dados de banco
    - DB_HOST deve ser o IP da sua máquina
    - DB_PORT deve ser 5432 para conectar ao bando criado pelo docker


Após fazer o clone, inicializar o docker com 
- docker-compose up -d

Para acessar via insomnia ou postman
- localhost:8080

Após subir ambiente, criar as tabelas com o comando
- php artisan migrate

Endpoints - os dados para cadastros/consultados são recebidos como 'query parameters'
    - Cadastrar Vendedor (POST) - /api/seller
        - query parameters: {
            name: string,
            email: string
        }
    - Cadastrar Venda (POST) - /api/sale
        - query parameters: {
            sellers_id: integer,
            sale_value: double,
        }

    - Consultar Vendedores (GET) - /api/seller
    - Consultar Vendas por Vendedor (GET) - /api/sale
        - query parameters: {
            sellers_id: integer
        }

Para visualisar o Cron para envio de relatório
- php artisan schedule:list

Para rodar os testes (deve ter criado algum vendedor/venda antes)
- php artisan test