<?php

use Illuminate\Support\Facades\Route;



Route::apiResource('/seller', 'api\SellerController');
Route::apiResource('/sale', 'api\SaleController');