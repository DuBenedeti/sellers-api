<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SaleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
   /** @test */
   public function get_sale()
   {
       $response = $this->get('/api/sale?sellers_id=1');
       
       $response->assertStatus(200);
    }
    
    /** @test */
   public function post_sale()
   {
       $response = $this->get('/api/sale?sellers_id=1&sale_value=288');
       
       $response->assertStatus(200);
   }
}
